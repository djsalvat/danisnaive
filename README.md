# DanIsNaIvE

My analysis code for NaIvE.
Consists of:
* stage2/ folder, containing a code to perform a second stage pass of the data.
* scripts/ folder, containing pyROOT scripts to perform calibrations and other post-processing
* submit/ folder, containing PBS job submission scripts for the CENPA-mamba cluster

## Authors

* **Daniel J. Salvat** - - [profile](https://code.ornl.gov/salvat)
