#PBS -N binary_converter_test
#PBS -l nodes=8
#PBS -l walltime=10:00:00        
#PBS -t 1-4
#PBS -m bea
#PBS -M salvat@uw.edu
cd /home/salvat/binaryConverter-NGM3302
python callBinaryConverter_multi.py 4 ${PBS_ARRAYID}
