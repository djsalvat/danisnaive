#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include <vector>
#include <climits>

#include "TFile.h"
#include "TTree.h"
#include "TH1D.h"
#include "TH2D.h"

#include <sqlite3.h>

using namespace std;

//this is our stage 2 event structure
struct stage2event
{
        Double_t E;
        Double_t ped;
        Double_t PSD;
        Double_t E_D;
        ULong64_t time;
        ULong64_t time_to_prev_39;
        ULong64_t time_to_next_39;
        ULong64_t time_to_prev_61;
        ULong64_t time_to_next_61;
        UShort_t amp;
        UShort_t amp_pos;
        UShort_t channelID;
        UShort_t pileup;
        Bool_t is_veto;
};

//this is our stage 3 event structure
struct stage3event
{
        Double_t E_1;
        Double_t E_2;
        ULong64_t time;
        ULong64_t time_to_prev_39;
        ULong64_t time_to_next_39;
        ULong64_t time_to_prev_61;
        ULong64_t time_to_next_61;
};

//define this type for convenience
typedef vector<struct stage2event> event_vector;

Int_t main(Int_t argc, char** argv)
{
        //	Print out usage information
        if(argc != 3)
        {
                printf("\nstage3\n");
                printf("Usage: stage3 <inputFileName> <outputFileName>\n");
                exit(0);
        }    

        //get the input file and stage 1 tree
        TString inputFilename(argv[1]);
        TFile* inFile = new TFile(inputFilename);
        TTree* stage2tree = (TTree*)inFile->Get("stage2tree");

        stage2event evt;
        stage2tree->SetBrancBranch("stage2event",&evt);

        TString outfileName(argv[2]);
        TFile *outFile;
        outFile = new TFile(outfileName,"RECREATE");
        TTree* stage3tree = new TTree("stage3tree",outfileName);
        //Set it to the current directory so it will be stored in there
        stage3tree->SetDirectory(outFile);

        time_t startTime, endTime; //start and end time variables
        time_t processStartingTime; //time variable to store the starting time
        time(&processStartingTime); //Get the current time, store in processStartingTime time variable

        //diagnostic histograms which I will also write to the file
        TH1D* htdiff[24][24];
        char title_tdiff[256];
        char name_tdiff[256];
        for (int i=0;i<24;i++)
        {
                for (int j=0;j<24;j++)
                {
                        sprintf(title_tdiff,"tdiff for chs %d %d",i,j);
                        sprintf(name_tdiff,"htdiff%d_%d",i,j);
                        htdiff[i][j] = new TH1D(name_ped,title_ped,16500,-1e,1e6);
                        htdiff[i][j]->SetDirectory(outFile);
                }
        }

        Long_t entries = stage2tree->GetEntries();
        ULong64_t last_veto_time,next_veto_time;
        for (int i=0;i<entries;i++)
        {
                //39 and 61 are in the last two channels
                stage2tree->GetEntry(i);
                stage3event e;

                if (evt.PSD < 0.02) continue;
                


                if (i % 1000000 == 0)
                {
                        cout << i << endl;
                }
        }
        inFile->Close();
        time(&endTime);

        cout << "Recorded " << i << " events in "
             << difftime(endTime,processStartingTime) << " seconds" << endl << endl;

        stage3tree->Write("stage3tree", TObject::kOverwrite);
        outFile->Write();
        outFile->Close();
}
