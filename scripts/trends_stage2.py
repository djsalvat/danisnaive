from sys import argv
from os import path
from datetime import datetime
import re
import sqlite3
import ROOT

ROOT.gStyle.SetOptStat(0)
#ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetPadLeftMargin(0.10)
ROOT.gStyle.SetPadRightMargin(0.05)
ROOT.gStyle.SetPadBottomMargin(0.1)
ROOT.gStyle.SetPadTopMargin(0.1)

guesses_K = {0:7700,
             1:6800,
             2:6200,
             3:6100,
             4:5200,
             5:5650,
             6:5800,
             7:5000,
             8:4800,
             9:3300,
            10:6400,
            11:7100,
            12:3900,
            13:3600,
            14:7000,
            15:7000,
            16:6300,
            17:5800,
            18:2500,
            19:6900,
            20:8400,
            21:8850,
            22:5050,
            23:8200}

guesses_Tl = {0:13450,
              1:11950,
              2:10850,
              3:10800,
              4:9150,
              5:10000,
              6:10100,
              7:8750,
              8:8500,
              9:5800,
             10:11150,
             11:12450,
             12:6950,
             13:6450,
             14:12300,
             15:12250,
             16:11100,
             17:10100,
             18:4450,
             19:12100,
             20:14700,
             21:15550,
             22:8800,
             23:14350}

class SQLCalibrationDatabase:
        def __init__(self,dbname):
                self.conn = sqlite3.connect(dbname)
                self.c = self.conn.cursor()
                self.c.execute('create table if not exists pedestals (run text, channel int, pedestal real, ped_err real)')
                self.c.execute('create table if not exists potassium (run text, channel int, K real, K_err real)')
                self.c.execute('create table if not exists thallium (run text, channel int, Tl real, Tl_err real)')
                self.commit()
        
        def __del__(self):
                self.conn.close()

        def insert_pedestal(self,runfile,channel,pedestal,sigma):
                self.c.execute('insert into pedestals values (?,?,?,?)',(runfile,channel,pedestal,sigma))

        def insert_potassium(self,runfile,channel,p,sigma):
                self.c.execute('insert into potassium values (?,?,?,?)',(runfile,channel,p,sigma))

        def insert_thallium(self,runfile,channel,p,sigma):
                self.c.execute('insert into thallium values (?,?,?,?)',(runfile,channel,p,sigma))

        def commit(self):
                self.conn.commit()

def get_datetime(filename):
        m = re.search('(?<=Raw_)[0-9]*',filename)
        the_string = m.group(0)
        datestrings = [the_string[0:4],the_string[4:6],the_string[6:8],the_string[8:10],the_string[10:12],the_string[12:14]]
        dates = tuple([int(ds) for ds in datestrings])
        return dates

if __name__=='__main__':
        #get files fed from the command line, as long as they are above a minimum size
        files = [f for f in argv[1:] if path.getsize(f) > 100*1024*1024]

        #connect to our sqlite database, which creates any tables which don't exist
        sqlcal = SQLCalibrationDatabase('/home/salvat/databases/trends.db')

        #create a run-by-run display, which may or may not be commented out
        #c_display = ROOT.TCanvas('c_display','c_display',1200,900)

        #this is a TGraphErrors for each quantity, for each channel. Used to make trend plots
        g_ped_dict = dict([(j,ROOT.TGraphErrors(len(files))) for j in range(24)])
        g_potassium_dict = dict([(j,ROOT.TGraphErrors(len(files))) for j in range(24)])
        g_thallium_dict = dict([(j,ROOT.TGraphErrors(len(files))) for j in range(24)])
        #make them datetime plots
        for k in g_ped_dict:
                g_ped_dict[k].GetXaxis().SetTimeDisplay(1)
                g_potassium_dict[k].GetXaxis().SetTimeDisplay(1)
                g_thallium_dict[k].GetXaxis().SetTimeDisplay(1)
        dt0 = ROOT.TDatime(*get_datetime(files[0]))
        ROOT.gStyle.SetTimeOffset(dt0.Convert())

        for n,fname in enumerate(files):
                print 'on file',fname
                the_file = ROOT.TFile(fname)

                #get time of this run relative to the first
                dt = ROOT.TDatime(*get_datetime(fname))
                t_ = dt.Convert()-dt0.Convert()

                #c_display.Divide(6,4)

                #get the pedestal histograms from the second stage pass
                ped_dict = dict([(j,getattr(the_file,'hped'+str(j))) for j in range(24)])
                for key in ped_dict:
                        ped_val = ped_dict[key].GetBinCenter(ped_dict[key].GetMaximumBin())
                        ped_err = ped_dict[key].GetStdDev()
                        g_ped_dict[key].SetPoint(n,t_,ped_val)
                        g_ped_dict[key].SetPointError(n,60*60*24,ped_err)

                        #insert the calibration
                        sqlcal.insert_pedestal(fname,key,ped_val,ped_err)

                #generate fit functions for the gamma lines for each channel
                fit_funcs_K = {}
                fit_funcs_Tl = {}

                #get the energy for each channel. project over time
                spectrum_dict = dict([(j,getattr(the_file,'hEvstime'+str(j))) for j in range(24)])
                for key in spectrum_dict:
                        #c_display.cd(key+1)
                        h = spectrum_dict[key].ProjectionY('hE'+str(key))

                        #set up the potassium fit based on a guess. same for Thallium below
                        g_K = guesses_K[key]
                        fit_funcs_K[key] = ROOT.TF1('fK_'+str(key),'[3]*exp(-[4]*x)+[0]/[2]*exp(-0.5*((x-[1])/[2])^2)',g_K-200.0,g_K+200.0)
                        fit_funcs_K[key].SetParameters(h.GetMaximum(),g_K,500.0,h.GetMaximum()/2.0,2000.0)
                        fit_funcs_K[key].SetParLimits(2,100.0,1000.0)
                        fit_funcs_K[key].SetLineColor(ROOT.kBlue)
                        h.Fit(fit_funcs_K[key],'QR')
                        p_K = fit_funcs_K[key].GetParameter(0)
                        p_K_err = fit_funcs_K[key].GetParameter(1)
                        g_potassium_dict[key].SetPoint(n,t_,p_K)
                        g_potassium_dict[key].SetPointError(n,60*60*24,p_K_err)

                        #insert the calibration
                        sqlcal.insert_potassium(fname,key,p_K,p_K_err)

                        g_Tl = guesses_Tl[key]
                        fit_funcs_Tl[key] = ROOT.TF1('fTl_'+str(key),'[3]*exp(-[4]*x)+[0]/[2]*exp(-0.5*((x-[1])/[2])^2)',g_Tl*0.95,g_Tl*1.05)
                        fit_funcs_Tl[key].SetParameters(h.GetMaximum(),g_Tl,500.0,h.GetMaximum()/2.0,2000.0)
                        fit_funcs_Tl[key].SetParLimits(2,100.0,1000.0)
                        fit_funcs_Tl[key].SetLineColor(ROOT.kMagenta)
                        h.Fit(fit_funcs_Tl[key],'QR+')
                        p_Tl = fit_funcs_Tl[key].GetParameter(0)
                        p_Tl_err = fit_funcs_Tl[key].GetParameter(1)
                        g_thallium_dict[key].SetPoint(n,t_,p_Tl)
                        g_thallium_dict[key].SetPointError(n,60*60*24,p_Tl_err)
                        sqlcal.insert_thallium(fname,key,p_Tl,p_Tl_err)

                        #h.GetXaxis().SetRangeUser(g_K-1000.0,g_Tl+1000.0)
                        #h.Draw()

                #raw_input()
                #c_display.Clear()

                #commit all additions for this runfile
                sqlcal.commit()
                the_file.Close()

        #plot the TGraphs to look at trends

        c_ped = ROOT.TCanvas('c_ped','c_ped',1200,900)
        c_ped.Divide(6,4)
        for key in g_ped_dict:
                c_ped.cd(key+1)
                g_ped_dict[key].Draw()

        c_potassium = ROOT.TCanvas('c_potassium','c_potassium',1200,900)
        c_potassium.Divide(6,4)
        for key in g_potassium_dict:
                c_potassium.cd(key+1)
                g_potassium_dict[key].Draw()

        c_thallium = ROOT.TCanvas('c_thallium','c_thallium',1200,900)
        c_thallium.Divide(6,4)
        for key in g_thallium_dict:
                c_thallium.cd(key+1)
                g_thallium_dict[key].Draw()

        raw_input()
