#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include <vector>
#include <climits>

#include "TFile.h"
#include "TTree.h"
#include "TH1D.h"
#include "TH2D.h"

using namespace std;

//this is our stage 2 event structure
struct stage2event
{
        Double_t E;
        Double_t ped;
        Double_t PSD;
        Double_t E_D;
        ULong64_t time;
        ULong64_t time_to_prev_39;
        ULong64_t time_to_next_39;
        ULong64_t time_to_prev_61;
        ULong64_t time_to_next_61;
        UShort_t amp;
        UShort_t amp_pos;
        UShort_t channelID;
        UShort_t pileup;
        Bool_t is_veto;
};

//define this type for convenience
typedef vector<struct stage2event> event_vector;

//just a helper function to sort out whether a detector is an outer veto
Bool_t check_veto(UShort_t ch)
{
        if (ch == 0 
         || ch == 5
         || ch == 8
         || ch == 9
         || ch == 13
         || ch == 15
         || ch == 18
         || ch == 21)
        {
                return kFALSE;
        }
        else
        {
                return kTRUE;
        }
}

Int_t main(Int_t argc, char** argv)
{
        //	Print out usage information
        if(argc != 3)
        {
                printf("\nstage2\n");
                printf("Usage: stage2 <inputFileName> <outputFileName>\n");
                exit(0);
        }    

        //get the input file and stage 1 tree
        TString inputFilename(argv[1]);
        TFile* inFile = new TFile(inputFilename);
        TTree* sis3302tree = (TTree*)inFile->Get("sis3302tree");

        UShort_t channelID;
        UShort_t cardID;
        ULong64_t timestamp;
        UShort_t eventHeader;
        UShort_t peakHighIndex;
        UShort_t peakHighValue;
        UInt_t accumulatorSum[8];
        UShort_t pileUpInfo;
        UShort_t nSamples;
        UShort_t waveform[65536];

        //Define branches for the tree
        sis3302tree->SetBranchAddress("channelID",&channelID);
        sis3302tree->SetBranchAddress("cardID",&cardID);
        sis3302tree->SetBranchAddress("timestamp",&timestamp);
        sis3302tree->SetBranchAddress("eventHeader",&eventHeader);
        sis3302tree->SetBranchAddress("peakHighIndex",&peakHighIndex);
        sis3302tree->SetBranchAddress("peakHighValue",&peakHighValue);
        sis3302tree->SetBranchAddress("accumulatorSum",accumulatorSum);
        sis3302tree->SetBranchAddress("pileUpInfo",&pileUpInfo);
        sis3302tree->SetBranchAddress("nSamples",&nSamples);
        sis3302tree->SetBranchAddress("waveform",waveform);

        TString outfileName(argv[2]);
        TFile *outFile;
        outFile = new TFile(outfileName,"RECREATE");
        TTree* stage2tree = new TTree("stage2tree",outfileName);
        //Set it to the current directory so it will be stored in there
        stage2tree->SetDirectory(outFile);

        time_t startTime, endTime; //start and end time variables
        time_t processStartingTime; //time variable to store the starting time
        time(&processStartingTime); //Get the current time, store in processStartingTime time variable

        //I make a vector of events, and stack them up until I've gone through a 39/61 window, and then flush them with the nearest 39/61 times
        event_vector events;
        stage2event evt;

        stage2tree->Branch("stage2event",&evt,"E/D:"
                                              "ped/D:"
                                              "PSD/D:"
                                              "E_D/D:"
                                              "time/l:"
                                              "time_to_prev_39/l:"
                                              "time_to_next_39/l:"
                                              "time_to_prev_61/l:"
                                              "time_to_next_61/l:"
                                              "amp/s:"
                                              "amp_pos/s:"
                                              "channelID/s:"
                                              "pileup/s:"
                                              "is_veto/O");

        //set the nearest 39/61 times to some unphysical min/max
        ULong64_t last_39,last_61;
        last_39 = last_61 = 0;
        ULong64_t next_39,next_61;
        next_39 = next_61 = ULONG_MAX;

        //diagnostic histograms which I will also write to the file
        TH1D* hped[24];
        TH2D* hEvstime[24];
        TH2D* hEgammavstime[24];
        char title_ped[256];
        char title_Evstime[256];
        char title_Egammavstime[256];
        char name_ped[256];
        char name_Evstime[256];
        char name_Egammavstime[256];
        for (int i=0;i<24;i++)
        {
                sprintf(title_ped,"pedestal for ch %d",i);
                sprintf(name_ped,"hped%d",i);
                sprintf(title_Evstime,"E vs time for ch %d",i);
                sprintf(name_Evstime,"hEvstime%d",i);
                sprintf(title_Egammavstime,"E_gamma vs time for ch %d",i);
                sprintf(name_Egammavstime,"hEgammavstime%d",i);
                hped[i] = new TH1D(name_ped,title_ped,16500,0,1e6);
                hEvstime[i] = new TH2D(name_Evstime,title_Evstime,48,0,pow(2,44),16500,0,600e3);
                hEgammavstime[i] = new TH2D(name_Egammavstime,title_Egammavstime,48,0,pow(2,44),16500,0,600e3);
                hped[i]->SetDirectory(outFile);
                hEvstime[i]->SetDirectory(outFile);
                hEgammavstime[i]->SetDirectory(outFile);
        }

        Long_t entries = sis3302tree->GetEntries();
        int i;
        for (i=0;i<entries;i++)
        {
                //if we've identified a next nearest 39 and 61, apply this time to all events in the buffer and flush them
                if (next_39 < ULONG_MAX && next_61 < ULONG_MAX)
                {
                        for (event_vector::iterator it=events.begin();it != events.end();++it)
                        {
                                evt = *it;
                                evt.time_to_prev_39 = evt.time-last_39;
                                evt.time_to_next_39 = next_39-evt.time;
                                evt.time_to_prev_61 = evt.time-last_61;
                                evt.time_to_next_61 = next_61-evt.time;
                                stage2tree->Fill();
                        }
                        event_vector().swap(events); //free vector and clear the memory by swapping with a new, empty vector
                        last_39 = next_39;
                        last_61 = next_61;
                        next_39 = next_61 = ULONG_MAX;
                }

                //39 and 61 are in the last two channels
                sis3302tree->GetEntry(i);
                if (channelID == 24)
                {
                        next_39 = timestamp;
                }
                else if (channelID == 25)
                {
                        next_61 = timestamp;
                }
                else
                {
                        stage2event e;
                        e.ped = 0.5*(accumulatorSum[0]+accumulatorSum[1]);
                        e.E = accumulatorSum[2]+accumulatorSum[3]-2.0*e.ped;
                        if (e.E > 0.0)
                        {
                                e.PSD = (accumulatorSum[3]-e.ped)/e.E;
                        }
                        else
                        {
                                e.PSD = 0.0;
                        }
                        e.E_D = (accumulatorSum[4]+accumulatorSum[5]+accumulatorSum[6]+accumulatorSum[7])-e.ped*4.0;
                        e.amp = peakHighValue;
                        e.amp_pos = peakHighIndex;
                        e.time = timestamp;
                        e.channelID = channelID;
                        e.pileup = pileUpInfo;
                        e.is_veto = check_veto(channelID);

                        hped[channelID]->Fill(e.ped);
                        hEvstime[channelID]->Fill(e.time,e.E);
                        if (0.02 < e.PSD && e.PSD < 0.10)
                        {
                                hEgammavstime[channelID]->Fill(e.time,e.E);
                        }

                        events.push_back(e);
                }

                if (i % 1000000 == 0)
                {
                        cout << i << endl;
                }
        }
        inFile->Close();
        time(&endTime);

        cout << "Recorded " << i << " events in "
             << difftime(endTime,processStartingTime) << " seconds" << endl << endl;

        stage2tree->Write("stage2tree", TObject::kOverwrite);
        outFile->Write();
        outFile->Close();
}
