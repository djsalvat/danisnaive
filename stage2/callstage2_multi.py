import os
import sys
from multiprocessing import Pool

#Location of in files (with trailing '/')
inFileLoc='/data/COHERENT/data/NaIvE/replay/'

#Location of out files (with trailing '/')--Used to see if the root file already exists, can ignore if it does
outFileLoc='/data/COHERENT/data/NaIvE/replay_stage2_3/'

#Full path to converter (with converter included)
converterLoc='/home/salvat/DanIsNaIvE/stage2/stage2'
if not os.path.isfile(converterLoc):
	print 'Path to converter not found!'
	print 'Make sure ' + converterLoc + ' exists!'
	sys.exit()

#Generate a list of all files in binFileLoc
allFiles = os.listdir(inFileLoc)

#Get in alphabetical (which is also chronological here) order
allFiles.sort()

#Out of those, we only care about the .bin files
filesToParse = [i for i in allFiles if i.endswith('.root')]

#Set to 1 to overwrite files
overwriteFiles = 0

#Now go through the list of files, check whether they need to be parsed.
#If they do, print out a message and parse them

runfiles = []
NUM_NODES = int(sys.argv[1])
PBS_ARRAYID = int(sys.argv[2])
print 'we are job',PBS_ARRAYID

total_size = 0
for currentFile in filesToParse:
	fileParts = currentFile.split('.')
	fileToCheck = fileParts[0]+'_stage2.root'
	if os.path.isfile(outFileLoc+fileToCheck) and overwriteFiles==0:
		print 'Parsed file ' + fileToCheck + ' found, skipping'
	else:
                fsize = os.path.getsize(inFileLoc+currentFile)
                total_size += fsize
		runfiles.append((fsize,(currentFile,fileToCheck)))

print 'found',len(runfiles),'files, with size',total_size

job_array_files = [[] for i in range(NUM_NODES)]
current_size = 0
for rf in runfiles:
        sz, (fin,fout) = rf
        current_size += sz
        INDEX = int(NUM_NODES*current_size/(total_size+1))
        job_array_files[INDEX].append((fin,fout))

the_files = job_array_files[PBS_ARRAYID-1]

def execute_converter(file_pair):
	infile, outfile = file_pair
	terminalCommand = converterLoc + ' ' + inFileLoc + infile + ' ' + outFileLoc + outfile
	os.system(terminalCommand)

p = Pool(8)
p.map(execute_converter,the_files)
